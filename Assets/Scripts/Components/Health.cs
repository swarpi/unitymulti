﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField]
    int maxHealth;
    int currentHealth;

    void Start(){
        currentHealth = maxHealth;
    }

    public void setMaxHealth(int maxHealth){
        this.maxHealth = maxHealth;
    }

    public int getMaxHealth(){
        return maxHealth;
    }

    public void setCurrentHealth(int currentHealth){
        this.currentHealth = currentHealth;
    }

    public int getCurrentHealth(){
        return currentHealth;
    }

    public bool TakeDamage(int damage){
        currentHealth -= damage;
        if(currentHealth > 0 ){
            return true;
        }else{
            onDie();
            return false;
        }

    }

    private void onDie(){
        // TODO implement what happens when you die
    }

}
