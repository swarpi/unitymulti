﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct PlayerTurnAction
{
    Spell spell;
    int accuracy;
    Tile target;

    public PlayerTurnAction(Spell spell, int accuracy, Tile target){
        this.spell = spell;
        this.accuracy = accuracy;
        this.target = target;
    }
    public Tile getTarget(){
        return target;
    }
    public void setTarget(Tile target){this.target = target;}
    public Spell getSpell(){
        return spell;
    }
    public void setSpell(Spell spell){this.spell = spell;}
    public int getAccuracy(){
        return accuracy;
    }
    public void setAccuracy(int accuracy){this.accuracy = accuracy;}
}
