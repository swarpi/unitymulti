﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Speed
{
    // speed to calculate how fast you are [0;1]
    double baseSpeed;
    // current amount of speed, if 100 its your turn [0;1]
    // TODO implement that turnspeed is always 0 at the beginning
    double turnSpeed;
    public Speed(double baseSpeed, double turnSpeed){
        this.baseSpeed = baseSpeed;
        this.turnSpeed = turnSpeed;

    }

    public double getBaseSpeed(){
        return baseSpeed;
    }
    public void setBaseSpeed(double baseSpeed){
        this.baseSpeed = baseSpeed;
    }
    public double getTurnSpeed(){
        return turnSpeed;
    }
    public void setTurnSpeed(double turnSpeed){
        if(turnSpeed < 100) {
            this.turnSpeed = turnSpeed;
        }else this.turnSpeed = 100;
        
    }

}
