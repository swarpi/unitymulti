﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile:MonoBehaviour
{
    string tileName;
    int x;
    int y;

    public Tile(int x, int y){
        this.x = x;
        this.y = y;
        tileName = "Tile_" + x + "_" + y;
    }

    public void setX(int x){
        this.x = x;
    }
    public int getX(){
        return x;
    }
    public void setY(int y){
        this.y = y;
    }
    public int getY(){
        return y;
    }
    public void setName(string tileName){
        this.tileName = tileName;
    }
    public string getName(){
        return tileName;
    }
}
