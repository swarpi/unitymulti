﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager_Fight1 : FightManager
{

    private static EnemyManager_Fight1 enemyManager_Fight1;
    public Soldat soldatBob;
    //public PlayerScript player;

    void Start(){
        
    }    
    private static EnemyManager_Fight1 _instance;

    public static EnemyManager_Fight1 Instance { get { return _instance; } }


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
        
    }

}
