﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Environment
{
    int tileSpace{get;set;}
    string name{get;set;}
}
