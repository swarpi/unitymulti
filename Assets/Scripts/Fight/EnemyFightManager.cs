﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFightManager : MonoBehaviour
{
    public Spell ChooseSpell(Enemy enemy){
        enemy.getSpellList();
        // pick a random spell from SpellList
        return enemy.getSpellList()[0];
    }

    public Player ChooseTargetToAttack(List<Player> allPlayers){

        return allPlayers[0];
    }
}
