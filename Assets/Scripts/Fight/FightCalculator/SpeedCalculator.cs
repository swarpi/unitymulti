﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedCalculator: MonoBehaviour
{
    List<int> allTurnSpeed;
    List<double> allBaseSpeed;


    public static Unit CalculateTurn(List<Unit> allUnits){
        while(true){
            foreach(Unit unit in allUnits){
                Speed unitSpeed = unit.getSpeed();
                double unitTurnSpeed = unitSpeed.getTurnSpeed();
                if(unitSpeed.getTurnSpeed() == 100){
                    return unit;
                }else{
                    unitSpeed.setTurnSpeed(unitTurnSpeed +unitSpeed.getBaseSpeed());
                }
            }
        }
    }
}
