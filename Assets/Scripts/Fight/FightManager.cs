﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAPI;
using System;
//using MLAPI.Connection;
public enum BattleState{
    START,
    PLAYERTURN,
    ENEMYTURN,
    PROCESSING,
    BLOCKING,
    ACTION,
    END,
   
}
public class FightManager : NetworkedBehaviour
{
    
    List<Unit> allUnits;
    // vlt List<Player> und List<Enemy>
    List<Environment> allEnvironmentObjects;
    BattleState state;
    List<Tile> enemyPosition;
    List<Enemy> allEnemies;
    bool isTurnfinished = false;
    

    void Start(){
        allUnits = new List<Unit>();
        state = BattleState.START;
        //TODO atm only gets playerprefab from server, but i want prefabs from client, so call method from client
        if(NetworkingManager.Singleton.IsServer) {
            
            foreach(var player in NetworkingManager.Singleton.NetworkConfig.NetworkedPrefabs){
                if(player.Equals(null)) continue;
                //Debug.Log(player.Prefab.gameObject.GetComponent<Player>().getName());
                allUnits.Add(player.Prefab.gameObject.GetComponent<Player>());
                //Debug.Log(player.Prefab.gameObject.GetComponent<Player>().getName());
                Debug.Log("added player");
            }
           
            //Debug.Log(NetworkingManager.Singleton.NetworkConfig.NetworkedPrefabs[0]);
            foreach(GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy")){
                Debug.Log(enemy);
                allUnits.Add(enemy.GetComponent<Enemy>());
                Debug.Log(enemy.GetComponent<Enemy>().getName());
                Debug.Log("added enemy");
                Debug.Log(allUnits.Count);
            }
            Debug.Log(allUnits[0].getName());
            Debug.Log(allUnits[1].getName());
           // Debug.Log(allUnits[2].getName());
           
            foreach(Unit unit in allUnits){
                if(unit == null) continue;
                Debug.Log(unit.getName());
            }
        }

            
        
        //allUnits.Add
        //TODO instantiate allPlayers.
        //allUnits.Add(Player oder Enemy)
        // enemyPosition.add(Enemy.Position)
        

    }

    void BattleRoutine(){
        while(state !=BattleState.END){
            // checks who's turn it is
            Unit unitsTurn = SpeedCalculator.CalculateTurn(allUnits);
            // set State right state
            if(unitsTurn.IsPlayer() == true) {
                state = BattleState.PLAYERTURN;
                PlayerTurn(unitsTurn);
                }
            else {
                state = BattleState.ENEMYTURN;
                EnemyTurn(unitsTurn);
                }


        }
    }

    IEnumerator PlayerTurn(Unit player){
        if(state != BattleState.PLAYERTURN) yield return 0;
        isTurnfinished = false;
        
        // notify client that it is his turn, set his isTurn to true
        
        // start Methods on PlayerUI, wait for the response
        // should be ClientRPC for client that owns the player

        // PlayerAction(player.PlayerFightManager.ActivateSpell());
        yield return new WaitUntil(() => isTurnfinished == true);
    }

    void PlayerAction(PlayerTurnAction PTA){
        // check if enemy is on Tile
        if(!enemyPosition.Contains(PTA.getTarget())){

            // Call Miss on MainScreen with UI
        }
        // find in enemy list enemy thats position is 
        Enemy targetedEnemy = allEnemies.Find(x => x.getPosition().Equals(PTA.getTarget()));
        int damageDealt = DamageCalculator.CalculateDamage(PTA.getSpell(),PTA.getAccuracy(),targetedEnemy);
        // Animation
        // targetedEnemy.Health.TakeDamage(damageDealt);
        // go back to BattleRoutine
        isTurnfinished = true;


    }
    void EnemyTurn(Unit enemy){
        if(state != BattleState.ENEMYTURN) return;

    }



}
