﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    public Transform tilePrefab;
    public Tile hexPrefab;
    public Vector2 mapSize;
    //to ouline the tiles (see that there are different tiles)
    [Range(0,1)]
    public float outLinePercent;
    float xOffset = 0.882f;
    float zOffset = 0.764f;

    void Start(){
        //GenerateMapSquare();
        GenerateMapHex();
    }


    public void GenerateMapSquare(){
        for(int x = 0; x < mapSize.x ; x ++){
            for(int y = 0; y <mapSize.y; y++){
                Vector3 tilePosition = new Vector3(-mapSize.x/2 + 0.5f + x ,0,-mapSize.y/2 + 0.5f + y);

                Transform newTile = Instantiate(tilePrefab,tilePosition,Quaternion.Euler(Vector3.right*90)) as Transform;
                //applies outlinePercent on new Tiles
                newTile.localScale = Vector3.one *(1-outLinePercent);
            }

        }

    }
        public void GenerateMapHex(){
        for(int x = 0; x < mapSize.x ; x ++){
            for(int y = 0; y <mapSize.y; y++){
                if(x == 6 && y%2 == 1 ) {
                    continue;
                }

                float xPos = x * xOffset;

                if(y % 2 == 1){
                    xPos += xOffset/2f;
                }
                
                Vector3 tilePosition = new Vector3(xPos,0,y*zOffset);

                Tile newTile = Instantiate(hexPrefab,tilePosition,Quaternion.identity);
                
                //newTile.name = "Hex_" + x + "_" + y;
                newTile.setX(y);
                newTile.setY(x);
                //puts Tiles into map
                newTile.transform.SetParent(this.transform);
                //applies outlinePercent on new Tiles
                //newTile.localScale = Vector3.one *(1-outLinePercent);
            }

        }

    }
}
