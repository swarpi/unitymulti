﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileManager : MonoBehaviour
{
    Tile selectedTile;
    Tile tempTile;
    guiTest gui;
    // Start is called before the first frame update
    void Start()
    {
        tempTile = new Tile(0,0);
    }
    
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;
        //passing by something by reference with out
        if(Physics.Raycast(ray, out hit)){
            GameObject ourHitObject = hit.collider.transform.gameObject;
            //only responds once you press on the button, holding wont work
            //maybe consider putting script on each tile, depends on performance
            if(Input.GetMouseButtonDown(0)){
                MeshRenderer mr = ourHitObject.GetComponentInChildren<MeshRenderer>();
                tempTile = hit.collider.transform.parent.GetComponent<Tile>();
                //Debug.Log(ourHitObject.GetComponents)
             
                mr.material.color = Color.red;
            } 
        }
    }

    public void ConfirmTile(){
        Debug.Log(tempTile.getX());
        selectedTile = tempTile;

        Debug.Log("selected Tile is " + selectedTile.getX());
        //return true;
    }
    
    public Tile getSelectedTile(){
        return selectedTile;
    }

    //TODO logic how to send the selected Tile to the FightManager, also need to be done for spell and Accuracy

}
