﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseManager : MonoBehaviour
{
    // Start is called before the first frame update
    guiTest gui;
    FightManager fight;
    void Start(){
    gui = guiTest.Instance;
    //fight = FightManager.Instance;
    }
    // Update is called once per frame
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;
        //passing by something by reference with out
        if(Physics.Raycast(ray, out hit)){
            GameObject ourHitObject = hit.collider.transform.gameObject;
            //only responds once you press on the button, holding wont work
            //maybe consider putting script on each tile, depends on performance
            if(Input.GetMouseButtonDown(0)){
                MeshRenderer mr = ourHitObject.GetComponentInChildren<MeshRenderer>();
                mr.material.color = Color.red;
                gui.tileClicker(hit.collider.transform.parent.name);
//                fight.notifyClientviaTile();
            } 
        }
    }
}
