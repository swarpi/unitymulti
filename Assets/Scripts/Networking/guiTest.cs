﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using MLAPI;
using MLAPI.Messaging;
using MLAPI.SceneManagement;
using UnityEngine.SceneManagement;

public class guiTest : NetworkedBehaviour
{
    private void Awake()
    {
        Debug.Log("starting");
        DontDestroyOnLoad(this.gameObject);
        mInstance = Instance;
        Debug.Log(NetworkingManager.Singleton.LocalClientId);
        //  
    }

    void Start(){
        
    }
    static guiTest mInstance;
 
    public static guiTest Instance
    {
        get
        {
            if(mInstance == null){
                mInstance = GameObject.FindObjectOfType<guiTest>();
            }
            return mInstance;
            //return mInstance ? null: (mInstance = (new GameObject("player")).AddComponent<guiTest>());
        }
    }

    private void OnGUI()
    {

        if (IsServer)
        {
            if (GUI.Button(new Rect(100, 100, 100, 100), "Change Scene to Test"))
            {
                // ruft Methode auf Clients aus
                InvokeClientRpcOnEveryone(MySwitchSceneRPC);
                
            }
                if (GUI.Button(new Rect(200, 200, 50, 50), "Change Scene to TileMap"))
            {
                // ruft Methode auf Clients aus
                InvokeClientRpcOnEveryone(MySwitchSceneRPCTile);
                SceneManager.LoadScene("TutorialFightEnemy");
            }

        }
        if (GUILayout.Button("SendRandomInt"))
        {

            Debug.Log(GetComponent<NetworkedObject>().GetInstanceID());
            if (IsServer)
            {
                InvokeClientRpcOnEveryone(MyClientRPC, Random.Range(-50, 50));
            }
            else
            {

                InvokeServerRpc(MyServerRPC, Random.Range(-50, 50));
            }
        }
    }

    public void callmyserver()
    {
        // ruft Methode auf dem Server auf
         InvokeServerRpc(MyServerRPC,22);
    }

    public void tileClicker(string name){
        Debug.Log("gui is client ? " + NetworkingManager.Singleton.IsClient);
        InvokeServerRpc(tileClickerToServer,name);
    }

    [ServerRPC]
    private void tileClickerToServer(string name){
        if(name.Equals("Hex_3_2")){
            Debug.Log("Enemy Hit");
            Health health = EnemyManager_Fight1.Instance.soldatBob.GetComponent<Health>();
            health.TakeDamage(20);
            
            Debug.Log( health.getCurrentHealth() + "is bobs new HP");
        }
        Debug.Log(name);
    }

    [ServerRPC]
    private void MyServerRPC(int number)
    {

        Debug.Log("Ownership is ?" + IsOwner);
        Debug.Log("Owned by client ?" + OwnerClientId);
        Debug.Log("is client ? " + IsClient + " and client id is " + NetworkId);
        Debug.Log("The number received was: " + number);
        Debug.Log("This method ran on the server upon the request of a client");
    }

    [ClientRPC]
    private void MySwitchSceneRPC()
    {
        Debug.Log("Change scene is called");
        SceneManager.LoadSceneAsync("Clientscene");
        //NetworkSceneManager.SwitchScene("Clientscene");
    }

    [ClientRPC]
    private void MySwitchSceneRPCTile()
    {
        Debug.Log("Change scene is called");
        SceneManager.LoadSceneAsync("TutorialFightPlayer");
        //NetworkSceneManager.SwitchScene("Clientscene");
    }

    [ClientRPC]
    private void MyClientRPC(int number)
    {
        Debug.Log("The number received was: " + number);
        Debug.Log("This method ran on the client upon the request of the server");
    }

    public void notifyClientviaTile(){
        InvokeClientRpcOnEveryone(notifyClientviaTileRPC);
    }
    [ClientRPC]
    private void notifyClientviaTileRPC(){
        Debug.Log("gui said its your turn");
    }



}

