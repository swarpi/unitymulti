﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAPI;

public class starterScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void startServerInit(){
        startServer();
    }

    private void startServer(){
        NetworkingManager.Singleton.StartServer();
    }
    public void startClientInit(){
        startClient();
    }

    private void startClient(){
        NetworkingManager.Singleton.StartClient();
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
