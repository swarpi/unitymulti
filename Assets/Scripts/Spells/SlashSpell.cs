﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlashSpell : MonoBehaviour,Spell
{
    SpellType spellType = SpellType.DAMAGESPELL;
    string description = "a normal slash";

    public SpellType getSpellType(){
        return spellType;
    }

    public string getDescription(){
        return description;
    }

}
