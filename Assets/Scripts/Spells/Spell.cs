﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum SpellType{
    DAMAGESPELL,
    HEALSPELL,
    BUFFSPELL,
    DEBUFFSPELL
}
public interface Spell
{
    SpellType getSpellType();
    string getDescription();
}