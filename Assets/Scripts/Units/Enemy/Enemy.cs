﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour, Unit
{
    [SerializeField]
    Tile position;
    List<Spell> spellList;
    [SerializeField]
    string name;
    [SerializeField]
    int id;
    [SerializeField]
    Health hp;
    [SerializeField]
    int armor;
    [SerializeField]
    Speed speed;


    public bool IsPlayer(){
        return false;
    }
    public Speed getSpeed(){
        return speed;
    }
    public Tile getPosition(){
        return position;
    }
    public void setPosition(Tile position){this.position = position;}
    public List<Spell> getSpellList(){
        return spellList;
    }
    public void setSpellList(List<Spell> spellList){this.spellList = spellList;}
    public string getName(){
        return name;
    }
}
