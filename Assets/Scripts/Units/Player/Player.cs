﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour, Unit
{
    [SerializeField]
    string playerName;
    List<Spell> spellList;
    int id;
    Health hp;
    int armor;
    Speed speed;
    public bool IsPlayer(){
        return true;
    }
    public Speed getSpeed(){
        return speed;
    }
    public string getName(){
        return playerName;
    }
    void Awake(){
        DontDestroyOnLoad(this.gameObject);
    }

    static Player mInstance;
 
    public static Player Instance
    {
        get
        {
            if(mInstance == null){
                mInstance = GameObject.FindObjectOfType<Player>();
            }
            return mInstance;
            //return mInstance ? null: (mInstance = (new GameObject("player")).AddComponent<guiTest>());
        }
    }
    

}
