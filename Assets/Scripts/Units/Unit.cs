﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Unit
{
    bool IsPlayer();
    Speed getSpeed();
    string getName();
}
